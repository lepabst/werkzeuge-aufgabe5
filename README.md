# How tun run

1. Make sure Java is installed and properly set up (environment variables).
2. Open the terminal/cmd and navigate to the folder containing this readme file.
3. Type javac HelloWorld.java to compile the java file.
4. Type java HelloWorld to run the compiled program.
5. You should receive "Moin Moin"!
